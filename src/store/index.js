
import {createStore} from "redux"


const initialState= {
    posts: []
}

function reducer(state = initialState, action){
    switch(action.type){
        case "ADD_POST" :  
            return {
                ...state,
                posts: [
                    ...state.posts,
                    action.payload
                ]
            };
        
        case "DELETE_POST" :  
            return {
                ...state,
                posts: state.posts.filter(post=>post.title !== action.payload)
            };

        default:
            return state    
    }


}


let store = createStore(reducer)

export default store