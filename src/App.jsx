
import './App.css'

import { useDispatch, useSelector } from "react-redux"

function App() {

  let { posts } = useSelector(state => state)

  const dispatch = useDispatch()

  console.log(posts);

  //add some value in store
  function addPost(title) {
    dispatch({
      type: "ADD_POST",
      payload: { title }
    })
  }

  // handle delete Post
  function deletePost(title) {
    dispatch({
      type: "DELETE_POST",
      payload: title
    })
  }


  return (

    <div>
      <h1>Hello</h1>
      <button onClick={() => addPost("fake title " + Date.now())}>Add Post</button>
      {posts.map(post => (
        <div key={post.title} style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
          <h4>{post.title}</h4>
          <button onClick={() => deletePost(post.title)}>Delete</button>
        </div>
      ))}
    </div>


  )
}

export default App
